<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class MerchantReturn extends Model
{
    protected $table = 'tb_merchant_return';
    
    public function getMerchant()
    {
        $data = MerchantReturn::leftJoin('tb_newlocker_groupname', 'groupname_id', '=', 'tb_newlocker_groupname.id')->get();

        $merchant = [];
        foreach ($data as $key => $value) {
            $merchant[] = $value->groupName;
        }
    
        return $merchant;
    }
}
