<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class ExpressBlock extends Model
{
    protected $table = 'tb_express_blocked';

    public function blocking($parcel_id, $validate_code)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;

        $expressBlockCheck = ExpressBlock::where('parcel_id', $parcel_id)->first();
        if ($expressBlockCheck) {
            if ($expressBlockCheck->is_blocking == 1) {
                $response->message = 'Data has been Blocked';
                return $response;
            } elseif ($expressBlockCheck->is_blocking == 0) {
                $response->message = 'Blocking data in a process';
                return $response;
            } else {
                $blocking = $this->blockingData($parcel_id, $validate_code, true);
                $response->isSuccess = $blocking->isSuccess;
                return $response;
            }
        } else {
            $blocking = $this->blockingData($parcel_id, $validate_code, false);
            $response->isSuccess = $blocking->isSuccess;
            return $response;
        }
    }

    private function blockingData($parcel_id, $validate_code, $is_update)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;

        DB::beginTransaction();
        try {

            $express = DB::table('tb_newlocker_express')->where('id',$parcel_id)->update([
                'validateCode' => 'BLOCKED'
            ]);

            if ($is_update) {
                $expressBlockCheck = ExpressBlock::where('parcel_id', $parcel_id)->first();
                $expressBlockCheck->is_blocking = 0;
                $expressBlockCheck->save();
                DB::commit();
                $response->isSuccess = true;
                return $response;
            } else {
                $expressBlock = new self();
                $expressBlock->parcel_id = $parcel_id;
                $expressBlock->old_validate_code = $validate_code;
                $expressBlock->is_blocking = 0;
                $expressBlock->save();
                
                DB::commit();
                $response->isSuccess = true;
                return $response;
            }
        } catch (\Exception $e) {
            DB::rollBack();
            $response->message = $e->getMessage();
            return $response;
        }
    }

    public function unblocking($parcel_id, $validate_code)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;

        $expressBlockCheck = ExpressBlock::where('parcel_id', $parcel_id)->first();
        if ($expressBlockCheck) {
            DB::beginTransaction();
            try {
                $express = DB::table('tb_newlocker_express')->where('id',$parcel_id)->update([
                    'validateCode' => $expressBlockCheck->old_validate_code
                ]);

                // $expressBlockCheck->is_blocking = 2;
                // $expressBlockCheck->save();
                DB::commit();
                $response->isSuccess = true;
                return $response;
            } catch (\Exception $e) {
                DB::rollBack();
                $response->message = $e->getMessage();
                return $response;
            }

        } else {
            $response->message = 'Data Not Found';
            return $response;
        }
    }

    public function callbackBlocking($express_id, $block_express_id)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $express = DB::table('tb_newlocker_express')->find($express_id);
        if ($express) {
            $blockExpress = ExpressBlock::find($block_express_id);
            if ($express->validateCode == "BLOCKED") {
                $blockExpress->is_blocking = 1;
                $blockExpress->save();
                $response->isSuccess = true;
                return $response;
            } else {
                $blockExpress->is_blocking = 2;
                $blockExpress->save();
                $response->isSuccess = true;
                return $response;
            }
        }
    }
}
