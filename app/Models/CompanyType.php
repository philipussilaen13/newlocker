<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyType extends Model
{
    protected $table = 'tb_master_company_type';

    public function getCompanyById($id_company)
    {
        $data = CompanyType::where('id', $id_company)->first();
        return $data;
    }
}
