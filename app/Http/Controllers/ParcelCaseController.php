<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Helpers\Helper;
use Illuminate\Support\Facades\DB;
use App\Http\Helpers\WebCurl;
use Illuminate\Support\Facades\Validator;
use App\Http\Helpers\PaymentAPI;
use App\Models\ExpressBlock;
use App\Models\ExpressPartners;
use App\Models\PopExpressTransit;
use App\Models\Express;

class ParcelCaseController extends Controller{
    var $curl;
    var $env_popsend_url;
    var $env_popsend_token;
    protected $internal_api;


    public function __construct(){
        $headers = ['Content-Type: application/json'];
        $this->curl = new WebCurl($headers);
        $this->env_popsend_url = env('POPSEND2_URL');
        $this->env_popsend_token = env('POPSEND2_TOKEN');
        $this->internal_api = env('API_INTERNAL');
    }

    // function from old app
    protected $headers = ['Content-Type: application/json'];
    protected $is_post = 0;
 
    
    public function post_data($url, $post_data = [], $headers = [], $options = []){
        $result = null;
        $curl = curl_init();

        if ((is_array($options)) && count($options) > 0) {
            $this->options = $options;
        }
        if ((is_array($headers)) && count($headers) > 0) {
            $this->headers = $headers;
        }
        if ($this->is_post !== null) {
            $this->is_post = 1;
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_POST, $this->is_post);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($curl, CURLOPT_COOKIEJAR, "");
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_ENCODING, "");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // required for https urls
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($curl, CURLOPT_TIMEOUT, 5);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 10);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->headers);

        $content = curl_exec($curl);
        $response = curl_getinfo($curl);
        $result = json_decode($content, TRUE);

        //debug


        curl_close($curl);
        return $result;
    }

    public function importedexpress(Request $req, $imported) {
        $boxToken = $req->header('BoxToken');
        $diskSerialNumber = $req->header('DiskSerialNumber');
        $orderNo = $req->header('OrderNo');
        $userToken = $req->header('UserToken');

        // cek di tabel express
        $sql = "SELECT * FROM tb_newlocker_express WHERE deleteFlag = '0' AND expressNumber = '".$imported."' AND expressType ='COURIER_STORE' AND status = 'IMPORTED' ORDER BY importTime DESC";
        $exp = DB::select($sql);

        if (count($exp) != 0 ) {
            $res =  ['additionalPayment' => [] , 'version' => $exp[0]->version, 'id' => $exp[0]->id, 'expressType' => $exp[0]->expressType, 'expressNumber' => $exp[0]->expressNumber, 'groupName' => $exp[0]->groupName, 'takeUserPhoneNumber' => $exp[0]->takeUserPhoneNumber ];
        } else {
            $res =  ['statusCode' => 404 , 'errorMessage' => 'express not found'];
        }

        //insert ke tabel generallog
        DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' =>  env('APP_URL').'/express/imported/' . $imported,
                    'api_send_data' => json_encode(['orderNo' => $orderNo, 'checkFor' => 'IMPORTED COURIER_STORE']),
                    'api_response' => json_encode($res),
                    'response_date' => date("Y-m-d H:i:s")
                ]);

        return response()->json($res);
    } 

    public function rejectcheckrule(Request $req, $imported) {
        $boxToken = $req->header('BoxToken');
        $diskSerialNumber = $req->header('DiskSerialNumber');
        $orderNo = $req->header('OrderNo');
        $userToken = $req->header('UserToken');

        //parsing express & groupname
        $express = $imported;
        $groupExpress = $_GET['type'];

        // cek rule di tabel return rule
        $sqlgr = "select * from tb_newlocker_returnrules where deleteFlag = '0' and groupName = '".$groupExpress."'";
        $rules = DB::select($sqlgr);
        $electronicCommerce_id = $rules[0]->ecommerce_id;
        $logisticsCompany_id = $rules[0]->logistic_id;
        $uniqueKey = '{{p0pb0x_4514}}';
        $canContinue = false;
        
        $ruleList = array();
        
        if (count($rules) > 1) {
            foreach ($rules as $rule) {
                array_push($ruleList, $rule->regularContent);           
            }
        }

        if (count($rules) > 1) {
            foreach ($ruleList as $rule_) {
                if (strpos($rule_, $groupExpress) !== false || is_array($rule_)) {
                    //cek di db apakah nomor express itu udah diupload dan memiliki groupname yang sama
                    $sql = "select * from tb_newlocker_express where deleteFlag = '0' and customerStoreNumber = '".$express."' and expressType = 'CUSTOMER_REJECT' and status = 'IMPORTED' and groupName = '" .$groupExpress. "'";
                    $exp = DB::select($sql);

                    if (count($exp) != 0) {
                        $canContinue = true;
                    } else {
                        $canContinue = false;
                    }
                } else {
                    //cek pola regex pada nomor express dengan key unik {{p0pb0x_4514}}
                    if (preg_replace('/'.$rule_.'/', $uniqueKey, $express) == $uniqueKey){
                        $canContinue = true;
                    }else{
                        $canContinue = false;
                    }
                }
                if ($canContinue) break;
            }
        } else if (count($rules) == 1) {
            $rule_ = $rules[0]->regularContent;
                if (strpos($rule_, $groupExpress) !== false || is_array($rule_)) {
                    //cek di db apakah nomor express itu udah diupload dan memiliki groupname yang sama
                    $sql = "select * from tb_newlocker_express where deleteFlag = '0' and customerStoreNumber = '".$express."' and expressType = 'CUSTOMER_REJECT' and status = 'IMPORTED' and groupName = '" .$groupExpress. "'";
                    $exp = DB::select($sql);

                    if (count($exp) != 0) {
                        $canContinue = true;
                    } else {
                        $canContinue = false;
                    }
                } else {
                    //cek pola regex pada nomor express dengan key unik {{p0pb0x_4514}}
                    if (preg_replace('/'.$rule_.'/', $uniqueKey, $express) == $uniqueKey){
                        $canContinue = true;
                    }else{
                        $canContinue = false;
                    }
                }
        } else {
            $canContinue = false;       
        }
        
        if ($canContinue) {
            //ambil data logistic company
            $sqlog = "select * from tb_newlocker_company where company_type = 'LOGISTICS_COMPANY' and id_company = '".$logisticsCompany_id."'";
            $rlog = DB::select($sqlog);

            $logisticsCompany_ = array('name' => $rlog[0]->company_name, 'id' => $rlog[0]->id_company);

            //ambil data ecommerce company
            $sqlecom = "select * from tb_newlocker_company where company_type = 'ELECTRONIC_COMMERCE' and id_company = '".$electronicCommerce_id."'";
            $rlecom = DB::select($sqlecom);
            $electronicCommerce_ = array('name' => $rlecom[0]->company_name, 'id' => $rlecom[0]->id_company, 'address' => $rlecom[0]->company_address);

            $res =  ['logisticsCompany' => $logisticsCompany_, 'groupName' => $groupExpress, 'electronicCommerce' => $electronicCommerce_]; 

        } else {

            $res =  ['statusCode' => 404 , 'errorMessage' => 'express reject rule not found for : '.$groupExpress ];
        }

        //insert ke tabel generallog
        DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' =>  env('APP_URL').'/express/reject/checkRule/' . $imported,
                    'api_send_data' => json_encode(['orderNo' => $orderNo, 'checkFor' => 'CUSTOMER_REJECT RULES']),
                    'api_response' => json_encode($res),
                    'response_date' => date("Y-m-d H:i:s")
                ]);

        return response()->json($res);
    }

    public function customerexpress(Request $req, $imported) {
        $boxToken = $req->header('BoxToken');
        $diskSerialNumber = $req->header('DiskSerialNumber');
        $orderNo = $req->header('OrderNo');
        $userToken = $req->header('UserToken');

        // cek di tabel express
        $express = new Express;
        $check_express = $express->expressChecking($req, $imported);

        if (!isset($check_express['statusCode']) == 404) {
            if ($check_express['parcelType'] == 'POPSEND') {
                $popsend = DB::connection('popsend')
                            ->table('delivery_destinations')
                            ->leftjoin('deliveries', 'delivery_destinations.delivery_id', '=', 'deliveries.id')
                            ->leftjoin('users', 'deliveries.user_id', '=', 'users.id')
                            ->where('invoice_sub_code', $imported)
                            ->first();
    
                if ($popsend) {
                    $check_express['sender'] = $popsend->name_sender;
                }
            } elseif ($check_express['parcelType'] == 'POPSAFE') {
                $popsafe = DB::connection('popsend')->table('popsafes')
                            ->leftJoin('users', 'popsafes.user_id', '=', 'users.id')
                            ->where('invoice_code', $imported)
                            ->first();
                
                if ($popsafe) {
                    $check_express['sender'] = $popsafe->name;
                    $expired = date("l, d F Y, H:i:s", strtotime($popsafe->expired_time));
                    $check_express['expired_time'] = $expired;
                }
            }

            $check_express["endAddress"] = str_replace("\n", " \n", $check_express["endAddress"]);

        }

        //insert ke tabel generallog
        DB::table('tb_newlocker_generallog')
        ->insert([
            'api_url' =>  env('APP_URL').'/express/customerExpress/' . $imported,
            'api_send_data' => json_encode(['orderNo' => $orderNo, 'checkFor' => 'IMPORTED CUSTOMER_STORE']),
            'api_response' => json_encode($check_express),
            'response_date' => date("Y-m-d H:i:s")
        ]);

        return response()->json($check_express);
    }   

    public function syncexpress(Request $req) {

        $id = $req->json('id');
        $expressNumber = $req->json('expressNumber');
        $expressType = $req->json('expressType');
        $takeUserPhoneNumber = $req->json('takeUserPhoneNumber');
        $status = $req->json('status');
        $groupName = $req->json('groupName');
        $customerStoreNumber = $req->json('customerStoreNumber');
        $overdueTime = $req->json('overdueTime');
        $storeTime = $req->json('storeTime');
        $syncFlag = $req->json('syncFlag');
        $takeTime = $req->json('takeTime');
        $storeUserPhoneNumber = $req->json('storeUserPhoneNumber');
        $validateCode = $req->json('validateCode');
        $version = $req->json('version');
        $box_id = $req->json('box_id');
        $logisticsCompany_id = $req->json('logisticsCompany_id');
        $mouth_id = $req->json('mouth_id');
        $operator_id = $req->json('operator_id');
        $storeUser_id = $req->json('storeUser_id');
        $takeUser_id = $req->json('takeUser_id');
        $recipientName = $req->json('recipientName');
        $weight = $req->json('weight');
        $chargeType = $req->json('chargeType');
        $electronicCommerce_id = $req->json('electronicCommerce_id');
        $staffTakenUser_id = $req->json('staffTakenUser_id');
        $endAddress = $req->json('endAddress');
        $status_op_taken = false;
        $smsSend = true;
        $courier_phone = $req->json('courier_phone');
        $other_company_name = $req->json('other_company_name');

        if (!$groupName) {
            $prefix = substr($expressNumber,0,3);
            $rpr = DB::table('tb_newlocker_groupname')->select('groupName', 'sms')->where('prefix', $prefix)->first();
        } else {
            $rpr = DB::table('tb_newlocker_groupname')->select('groupName', 'sms')->where('groupName', $groupName)->first();
        }

        if ($rpr) {
            $groupName = $rpr->groupName;
            $smsSend = $rpr->sms;
        }else{
            $groupName = 'UNDEFINED';
        }

        if ($req->json('paymentParam') != null || $req->json('paymentOverdue') != null) {
            $msg = $expressNumber ."- : ".$req->getContent()." \n";
            $name = 'sync';
            $this->writeLogs($msg, $name);
            $dataParam = [
                'paymentParam' => $req->json('paymentParam'),
                'paymentOverdue' => $req->json('paymentOverdue')
            ];
            
            foreach ($dataParam as $key => $paramRequest) {
                $msg = $expressNumber ."- $key - : $paramRequest \n";
                $name = 'sync';
                $this->writeLogs($msg, $name);
                $paymentParam = json_decode(json_decode($paramRequest));
                $lockerIdentity = $req->json('locker_identity');
                $checkProperty = property_exists((object)$paymentParam, 'payment_offline');
                
                if ($checkProperty || $status == 'CUSTOMER_TAKEN' || $status == 'OPERATOR_TAKEN') {
                    $paymentOffline = $checkProperty ? (bool)$paymentParam->payment_offline : null;
                    $checkExpress = $this->checkExpressNumber($expressNumber);
                    // dd($checkExpress);
                    $extend = false;
                    if ($req->json('dataExtend') != null) {
                        $dataExtend = json_decode(json_decode($req->json('dataExtend')));
                        $extend = $dataExtend->isExtend;
                    }
                //    dd(($key == 'paymentParam' && $checkExpress == null) || ($key == 'paymentParam' && $extend == false));
                    if ($paymentOffline || $status == 'CUSTOMER_TAKEN' || $status == 'OPERATOR_TAKEN') {
                        if (!empty($paymentOffline)) {
                            $pay = $this->createPayment($paymentParam, $expressNumber);
                            $msg = $expressNumber ."- $key - : $pay \n";
                            $name = 'payment';
                            $this->writeLogs($msg, $name);
                        }
                       if (($key == 'paymentParam' && $checkExpress == null) || ($key == 'paymentParam' && $extend == false)) {
                            $popsafe = $this->createPopsafe($req, $paymentParam, $lockerIdentity, false, $status_op_taken);
                            
                            $msg = $expressNumber ."- $key - CREATED : $popsafe \n";
                            $name = 'popsafe';
                            $this->writeLogs($msg, $name);
                       } elseif (($key == 'paymentOverdue' && $checkExpress != null && $req->json('paymentOverdue') != null) || $extend == true || $status == 'OPERATOR_TAKEN') {
                            if ($status == 'OPERATOR_TAKEN' || $status == 'COURIER_TAKEN') {
                                $status_op_taken = true;
                            }
                            $popsafe = $this->createPopsafe($req, $paymentParam, $lockerIdentity, true, $status_op_taken);  
                            
                            $msg = $expressNumber ."- $key - EXTEND : $popsafe \n";
                            $name = 'popsafe';
                            $this->writeLogs($msg, $name);
                       }
                    } 
                }
            }
        }
        // dd('a');

        $url_ = null;
        $param_ = null;
        $resp = null;

        //cek apakah ada di database :
        $sqlc = "SELECT id,logisticsCompany_id  FROM tb_newlocker_express WHERE id='".$id."'";
        $rc = DB::select($sqlc);
        
        if (count($rc) != 0 ) {
            $logisticsCompany_id = !empty($rc[0]->logisticsCompany_id) ? $rc[0]->logisticsCompany_id : $logisticsCompany_id;
            
            $expressPartners = new ExpressPartners;
            $sendCallback = $expressPartners->updateData($id, $status);
            
            //update dataupdateT
            DB::table('tb_newlocker_express')
            ->where('id', $id)
                    ->update(array(
                        'expressNumber' => $expressNumber,
                        'expressType' => $expressType,
                        'takeUserPhoneNumber' => $takeUserPhoneNumber,
                        'status' => $status,
                        'groupName' => $groupName,
                        'customerStoreNumber' => $customerStoreNumber,
                        'overdueTime' => $overdueTime,
                        'storeTime' => $storeTime,
                        'syncFlag' => $syncFlag,
                        'takeTime' => $takeTime,
                        'storeUserPhoneNumber' => $storeUserPhoneNumber,
                        'validateCode' => $validateCode,
                        'version' => $version,
                        'box_id' => $box_id,
                        'logisticsCompany_id' => $logisticsCompany_id,
                        'mouth_id' => $mouth_id,
                        'operator_id' => $operator_id,
                        'storeUser_id' => $storeUser_id,
                        'takeUser_id' => $takeUser_id,
                        'recipientName' => $recipientName,
                        'weight' => $weight,
                        'chargeType' => $chargeType,
                        'electronicCommerce_id' => $electronicCommerce_id,
                        'staffTakenUser_id' => $staffTakenUser_id,
                        'lastModifiedTime' => time() * 1000

            ));
        } else {
            //insert data
            DB::table('tb_newlocker_express')
                    ->insert([
                            'expressNumber' => $expressNumber,
                            'expressType' => $expressType,
                            'takeUserPhoneNumber' => $takeUserPhoneNumber,
                            'status' => $status,
                            'groupName' => $groupName,
                            'customerStoreNumber' => $customerStoreNumber,
                            'overdueTime' => $overdueTime,
                            'storeTime' => $storeTime,
                            'syncFlag' => $syncFlag,
                            'takeTime' => $takeTime,
                            'storeUserPhoneNumber' => $storeUserPhoneNumber,
                            'validateCode' => $validateCode,
                            'version' => $version,
                            'box_id' => $box_id,
                            'logisticsCompany_id' => $logisticsCompany_id,
                            'mouth_id' => $mouth_id,
                            'operator_id' => $operator_id,
                            'storeUser_id' => $storeUser_id,
                            'takeUser_id' => $takeUser_id,
                            'recipientName' => $recipientName,
                            'weight' => $weight,
                            'chargeType' => $chargeType,
                            'electronicCommerce_id' => $electronicCommerce_id,
                            'staffTakenUser_id' => $staffTakenUser_id,
                            'id' => $id,
                            'lastModifiedTime' => time() * 1000

             ]);
        }

        //update tabel mouth
        if($status=='IN_STORE'){
            DB::table('tb_newlocker_mouth')
                ->where('id_mouth', $mouth_id)
                    ->update(array(
                            'status' => 'USED',
                            'express_id' => $id,
                            'syncFlag' => 1,
                            'lastChangingTime' => time() * 1000
                        ));
        } else {
            DB::table('tb_newlocker_mouth')
                ->where('id_mouth', $mouth_id)
                    ->update(array(
                            'status' => 'ENABLE',
                            'express_id' => null,
                            'syncFlag' => 1,
                            'lastChangingTime' => time() * 1000
                        ));            
        }

        //ambil data logistic company
        $sqlog = "SELECT * FROM tb_newlocker_company WHERE id_company = '".$logisticsCompany_id."'";
        $rl = DB::select($sqlog);

        //ambil data store user
        $sqlu = "SELECT * FROM tb_newlocker_user WHERE id_user='".$storeUser_id."'";
        $ru = DB::select($sqlu);

        if (!($ru)) {
            $ru = DB::table('tb_newlocker_user')->insert([
                'id_user' => $storeUser_id,
                'phone' => $courier_phone,
                'id_company' => $logisticsCompany_id,
                'user_type' => $rc->company_name,
                'groupname' => $rc->company_name,
                'role' => 'LOGISTICS_COMPANY_USER',
                'username' => $courier_phone,
                'displayname' => $courier_phone,
                'registered_other_company' => $other_company_name,
                'password' => \hash('sha256', Helper::generateRandomString(10).'_POPBOX_NEWLOCKER_SALT'),
                'id_role' => 6
            ]);
        }

        if (count($ru) != 0) {
            $storeUser = array('id'=> $storeUser_id, 'userCardList' => ['id'=>''], 'name'=> $ru[0]->displayname, 'loginName' => $ru[0]->username, 'phoneNumber' => $ru[0]->phone, 'userNo' => '');
        } else {
            $storeUser = array('id'=> $storeUser_id, 'userCardList' => ['id'=>''], 'name'=> $storeUser_id, 'loginName' => $storeUser_id, 'phoneNumber' => $storeUserPhoneNumber, 'userNo' => '');
        }

        //ambil data mouth
        $sqlm = "SELECT * FROM tb_newlocker_mouth WHERE id_mouth='".$mouth_id."'";
        $rm = DB::select($sqlm);

        //ambil data mouth type
        $sqlmt = "SELECT * FROM tb_newlocker_mouthtype WHERE id_mouthtype='".$rm[0]->mouthType_id."'";
        $mt = DB::select($sqlmt);

        //ambil data box
        $sqlb  = "SELECT * FROM tb_newlocker_box WHERE id = '".$box_id."'";
        $rb = DB::select($sqlb);

        $box = array('id' => $box_id, 'orderNo' => $rb[0]->orderNo, 'name' => $rb[0]->name ); 

        $mouthType = array('defaultOverduePrice' => $mt[0]->defaultOverduePrice, 'id' => $mt[0]->id_mouthtype, 'defaultUserPrice' => $mt[0]->defaultUserPrice, 'deleteFlag' => $mt[0]->deleteFlag, 'name' => $mt[0]->name);

        $mouth = array('mouthType' => $mouthType, 'id' => $mouth_id, 'box' => $box , 'status' => $rm[0]->status , 'number' => $rm[0]->number);

        //ambil data take user
        $loginName = 'C_'.$takeUserPhoneNumber;
        $takeUser = array('id'=> $staffTakenUser_id, 'userCardList' => ['id'=>''], 'name'=> $takeUserPhoneNumber, 'loginName' => $loginName, 'phoneNumber' => $takeUserPhoneNumber, 'userNo' => '');

        $logistic = array('id' => $logisticsCompany_id, 'contactPhoneNumber' => [], 'contactEmail' => [], 'name' => $rl[0]->company_name, 'companyType' => $rl[0]->company_type, 'deleteFlag' => $rl[0]->deleteFlag, 'level' => $rl[0]->level);

        //get data operator
        $sqlop = "SELECT * FROM tb_newlocker_company WHERE id_company = '".$operator_id."'";
        $rop = DB::select($sqlop);

        $operator = array('id' => $operator_id, 'contactPhoneNumber' => [], 'contactEmail' => [], 'name' => $rop[0]->company_name, 'companyType' => $rop[0]->company_type, 'deleteFlag' => $rop[0]->deleteFlag, 'level' => $rop[0]->level);

        if($expressType == 'COURIER_STORE') {
            $res =  ['validateCode' => $validateCode , 'storeUser' => $storeUser, 'mouth' => $mouth, 'takeUser' => $takeUser, 'storeTime' => $storeTime, 'takeTime' => $takeTime, 'expressType' => $expressType, 'status' => $status , 'items' => [] , 'version' => $version, 'expressNumber' => $expressNumber, 'logisticsCompany' => $logistic, 'id' => $id ,'box' => $box, 'additionalPayment' => [], 'includedNumbers' => 0, 'takeUserPhoneNumber' => $takeUserPhoneNumber, 'overdueTime' =>  $overdueTime, 'operator' => $operator]; 
            $url_ = $this->internal_api.'/synclocker/courierstore';
            $param_ = json_encode($res);
            $resp = $this->post_data($url_, $param_);   

            if($groupName == "POPDEPOSIT" || substr($expressNumber, 0, 3) == "PDS"){
                $url__ = env('POPSEND2_URL').'popsafe/updateStatus';
                $param__ = ["token" => $this->env_popsend_token, "invoice_id" => $expressNumber, "status" => $status,
                    "locker_number" => $rm[0]->number, "code_pin" => $validateCode, "parcel_id" => $id,
                    "remarks" => "Updated from pr0x : ". date("Y-m-d H:i:s")];
                $resp__ = $this->post_data($url__, json_encode($param__));   
                //log push to popsend2 server
                DB::table('tb_newlocker_generallog')->insert(
                    ['api_url' =>  $url__, 
                    'api_send_data' => json_encode($param__),
                    'api_response' => json_encode($resp__),
                    'response_date' => date("Y-m-d H:i:s")]); 
            }
        }
        
        if($expressType == 'CUSTOMER_REJECT') {
            //ambil data ecommerce company
            $sqecom = "SELECT * FROM tb_newlocker_company WHERE id_company = '".$electronicCommerce_id."'";
            $recom = DB::select($sqecom);

            $electronicCommerce = array('id' => $electronicCommerce_id, 'contactPhoneNumber' => [], 'contactEmail' => [], 'name' => $recom[0]->company_name, 'companyType' => $recom[0]->company_type, 'deleteFlag' => $recom[0]->deleteFlag, 'level' => $recom[0]->level);

            $res =  ['storeUser' => $storeUser, 'mouth' => $mouth, 'takeUser' => $takeUser, 'storeTime' => $storeTime, 'takeTime' => $takeTime, 'status' => $status , 'version' => $version, 'customerStoreNumber' => $customerStoreNumber, 'logisticsCompany' => $logistic, 'id' => $id ,'box' => $box, 'electronicCommerce' => $electronicCommerce, 'operator' => $operator]; 

            $url_ = $this->internal_api.'/synclocker/customereject';
            $param_ = json_encode($res);
            $resp = $this->post_data($url_, $param_);
        } 

        if($expressType == 'CUSTOMER_STORE') {
            $res =  ['storeUser' => $storeUser, 'mouth' => $mouth, 'takeUser' => $takeUser, 'storeTime' => $storeTime, 'takeTime' => $takeTime, 'expressType' => $expressType, 'status' => $status , 'items' => [] , 'version' => $version, 'customerStoreNumber' => $customerStoreNumber, 'logisticsCompany' => $logistic, 'id' => $id ,'box' => $box, 'additionalPayment' => [], 'includedNumbers' => 0, 'takeUserPhoneNumber' => $takeUserPhoneNumber, 'operator' => $operator, 'endAddress' => $endAddress, 'recipientName' => $recipientName]; 
            
            $url_ = $this->internal_api.'/synclocker/customerstore';
            $param_ = json_encode($res);
            $resp = $this->post_data($url_, $param_);
        }

        $cekSms = $this->checkSms($id);
        if (empty($cekSms) && ($smsSend == true || $smsSend == 1)){
            if ($operator_id == '145b2728140f11e5bdbd0242ac110001') {
                $overduetimesms = date('d/m/y H:i', $overdueTime / 1000);
                $param = array('box_name' => $box['name'], 'validateCode' => $validateCode, 'overduetimesms' => $overduetimesms, 'expressNumber' => $expressNumber, 'operator_id' => $operator_id);
                $message = $this->sms_content($groupName, $param); 
                $resp = $this->sendFromNexmo($takeUserPhoneNumber, $message); 
                $resp = $resp->getData();
                $response = property_exists($resp, 'response');
                if ($response) {
                    if ($resp->response->code != 200) {
                        $statusMsg = "FAILED";
                    }
                } else {
                    $statusMsg = (strpos(str_replace(' ', '', $resp), '"status":"0"') != false) ? 'SUCCESS' : 'FAILED' ;
                }
                // if ($resp->response->code == 200 || (strrpos($resp, '"status":"0"') !== false)) {
                DB::table('tb_newlocker_smslog')
                ->insert([
                    'express_id' => $id,
                    'sms_content' => '[Auto] '.$message,
                    'sms_status' => $statusMsg,
                    'sent_on' => date("Y-m-d H:i:s"),
                    'original_response' => json_encode($resp)
                    ]); 
                
                $msg = $expressNumber ." - sms :". json_encode($resp) ."\n";
                $name = 'sms';
                $this->writeLogs($msg, $name);
                
            } else {
                $urlsms = "http://pr0x-my.popbox.asia/task/express/resendSMS";        
                if (!in_array('userToken', $this->headers)) {
                    $this->headers[]='userToken:22cc0a50e199494682a9fc2ee2e88294';
                }  
                $this->post_data($urlsms, json_encode(['id' => $id]), $this->headers);
            }
        }

        if ($groupName == 'POPEXPRESS-TRANSIT') {
            $data = [
                "parcel_id" => $id,
                "expressNumber" => $expressNumber,
                "pin_code" => $validateCode,
                "locker_door" => $mouthType['name']."/".$mouth['number'],
                "locker_name" => $box['name'],
                "store_time" => date('Y-m-d H:i:s', $storeTime / 1000),
                "expired_time" => date('Y-m-d H:i:s', $overdueTime / 1000),
                "status" => "IN_STORE",
                "username" => $storeUser['loginName'],
                "courier_name" => $storeUser['name']
            ];
            
            $expressTransit = new PopExpressTransit;
            $transit = $expressTransit->transit($data);     
        }

        //insert ke tabel generallog
        DB::table('tb_newlocker_generallog')
            ->insert([
                [   'api_url' =>  env('APP_URL').'/express/syncExpress',
                    'api_send_data' => json_encode(['id' => $id, 'expressType' => $expressType, 'expressNumber' => $expressNumber, 'customerStoreNumber' => $customerStoreNumber]),
                    'api_response' => json_encode($res),
                    'response_date' => date("Y-m-d H:i:s")],
                [   'api_url' =>  $url_,
                    'api_send_data' => $param_,
                    'api_response' => json_encode($resp),
                    'response_date' => date("Y-m-d H:i:s")]
                ]);

        return response()->json($res);
    }

    //upload data popsend order ke locker 
    public function importcustomerstore (Request $req) {
        $id = $req->json('id');
        $logistic_id = $req->json('logistic_id');
        $takeUserPhoneNumber = $req->json('takeUserPhoneNumber');
        $customerStoreNumber = $req->json('customerStoreNumber');
        $chargeType = $req->json('chargeType');
        $recipientName = $req->json('recipientName');
        $recipientUserPhoneNumber = $req->json('recipientUserPhoneNumber');
        $endAddress = $req->json('endAddress');
        $designationSize = $req->json('designationSize');
        $groupName = $req->json('groupName');
        
        //Fill up GroupName
        $prefix = substr($customerStoreNumber, 0, 3);
        
        if ($prefix == "PLA" || $prefix == "PLL" || $prefix == "PAL" ) {
            $groupName = "POPSEND";
        } 

        if (empty($groupName)) {
            $sqlpr = "SELECT * FROM tb_newlocker_groupname WHERE prefix='".$prefix."'";
            $rpr = DB::select($sqlpr);

            //kasih handling saat get data groupname dan ditandain, mungkin belum diinput di DB
            if (count($rpr) != 0) {
                $groupName = $rpr[0]->groupName;            
            }else{
                $groupName = 'UNDEFINED';
            }
        }


        if(empty($logistic_id)){
            switch ($groupName) {
                case 'POPSEND':
                     $logistic_id == "161e5ed1140f11e5bdbd0242ac110001";
                    break;
                case 'TAYAKA':
                      $logistic_id == "402880825895e53e01589a50f9763b72";
                     break;     
                case 'OMAISU':
                      $logistic_id == "402880825895e53e01589a5030e63b5c";
                     break; 
                case 'TAPTOPICK':
                      $logistic_id == "40288083566d7b7f01568bcf5e1b703f";
                     break;  
                case 'VCS':
                      $logistic_id == "40288087598c3f230159ed4b9dbc67ce";
                     break;   
                case 'LABALABA':
                      $logistic_id == "402990835a20e7f0015a2127b4060630";
                     break; 
                case 'LOTG':
                      $logistic_id == "4028808258a8efaa0158adb7de3102d9";
                     break;         
                default:
                    $logistic_id == "161e5ed1140f11e5bdbd0242ac110001";
                    break;
             } 
        }

        //insert ke tabel express untuk popsend
        DB::table('tb_newlocker_express')
                ->insert([
                    'id' =>  $id,
                    'expressType' => 'CUSTOMER_STORE',
                    'customerStoreNumber' => $customerStoreNumber,
                    'takeUserPhoneNumber' => $takeUserPhoneNumber,
                    'status' => 'IMPORTED',
                    'groupName' => $groupName,
                    'importTime' => time() * 1000,
                    'lastModifiedTime' => time() * 1000,
                    'syncFlag' => 0,
                    'deleteFlag' => 0,
                    'version' => 0,
                    'logisticsCompany_id' => $logistic_id,
                    'endAddress' => $endAddress,
                    'recipientName' => $recipientName,
                    'recipientUserPhoneNumber' => $recipientUserPhoneNumber,
                    'designationSize' => $designationSize,
                    'chargeType' => $chargeType
        ]);

        $res = ['response'=> ['code' => 200, 'message' => 'OK'],  'data' => ['id' => $id, 'status' => 'IMPORTED']];

        //insert ke tabel generallog
        DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' =>  env('APP_URL').'/express/staffImportCustomerStoreExpress',
                    'api_send_data' => json_encode(['id' => $id, 'customerStoreNumber' => $customerStoreNumber, 'expressType' => 'CUSTOMER_STORE']),
                    'api_response' => json_encode($res),
                    'response_date' => date("Y-m-d H:i:s")
                ]);
                
        return response()->json($res); 
    }

    //insert ke tabel express untuk lastmile
    public function importcourierstore(Request $req) {
        $id = $req->json('id');
        $expressNumber = $req->json('expressNumber');
        $takeUserPhoneNumber = $req->json('takeUserPhoneNumber');
        $groupName = $req->json('groupName');
        $logistic_id = $req->json('logisticCompany');
        
        //set groupname dari prefix
        if (empty($groupName) || $groupName == "") {
            $prefix = substr($expressNumber,0,3);
            $sqlpr = "select id, groupName from tb_newlocker_groupname where prefix='".$prefix."'";
            $rpr = DB::select($sqlpr);

            //kasih handling saat get data groupname dan ditandain, mungkin belum diinput di DB
            if (count($rpr) == 0) {
                $groupName = 'UNDEFINED';
            }else{
                $groupName = $rpr[0]->groupName;            
            }
        }

        $takeUserPhoneNumber = preg_replace('/[^0-9]/', '', $takeUserPhoneNumber);

        //insert ke tabel express after check
        $sql = "SELECT * FROM tb_newlocker_express WHERE status = 'IMPORTED' and expressNumber = '".$expressNumber."'";
        $r = DB::select($sql);
        $generated_id = (!empty($id)) ? $id : hash('haval128,5', (time()*1000));

        if (count($r) != 0){

            if ($groupName=="COD"){
                DB::table('tb_newlocker_express')
                ->insert([
                    'id' =>  $generated_id,
                    'expressType' => 'COURIER_STORE',
                    'status' => 'IMPORTED',
                    'expressNumber' => $expressNumber,
                    'takeUserPhoneNumber' => $takeUserPhoneNumber,
                    'groupName' => $groupName,
                    'importTime' => time() * 1000,
                    'lastModifiedTime' => time() * 1000,
                    'logisticsCompany_id' => $logistic_id,
                    'syncFlag' => 1,
                    'deleteFlag' => 0
                ]);
                $res = ['response'=> ['code' => 200, 'message' => 'OK'],  'data' => ['id' => $generated_id, 'status' => 'IMPORTED']];  
            } else {
                $res = ['response'=> ['code' => 401, 'message' => 'DUPLICATE ENTRY'],  'data' => []];
            }

        }else{

            try {
                DB::table('tb_newlocker_express')
                    ->insert([
                        'id' =>  $generated_id,
                        'expressType' => 'COURIER_STORE',
                        'status' => 'IMPORTED',
                        'expressNumber' => $expressNumber,
                        'takeUserPhoneNumber' => $takeUserPhoneNumber,
                        'groupName' => $groupName,
                        'importTime' => time() * 1000,
                        'lastModifiedTime' => time() * 1000,
                        'logisticsCompany_id' => $logistic_id,
                        'syncFlag' => 1,
                        'deleteFlag' => 0
                    ]);

                $expressPartnerDb = new ExpressPartners;
                $expressPartner = $expressPartnerDb->add($req, 'IMPORTED'); 

                $res = ['response'=> ['code' => 200, 'message' => 'OK'],  'data' => ['id' => $generated_id, 'status' => 'IMPORTED']];            
            } catch (\Exception $e) {
                $res = ['response'=> ['code' => 500, 'message' => $e->errorInfo[2]],  'data' => ['id' => $generated_id, 'status' => 'IMPORT DATA FAILED']]; 
            }

        }

        //insert ke tabel generallog
        DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' =>  env('APP_URL').'/express/import',
                    'api_send_data' => json_encode(['id' => $generated_id, 'expressNumber' => $expressNumber, 'expressType' => 'COURIER_STORE']),
                    'api_response' => json_encode($res),
                    'response_date' => date("Y-m-d H:i:s")
                ]);

        return response()->json($res); 
    }  

        /*====================NON-LOCKER API======================*/
    //For below these non-locker API(s), need to put usertoken on Header as mandatory
    public function modifyPhoneNumber (Request $req) {
        $userToken = $req->header('UserToken');
        $sqltok = "SELECT count(*) AS token FROM tb_newlocker_token WHERE deleteFlag = '0' AND token ='".$userToken."'";
        $rtok = DB::select($sqltok);
        $granted = $rtok[0]->token;
        $id = $req->json('id');
        $takeUserPhoneNumber = $req->json('takeUserPhoneNumber');
        $expressNumber = null;

        if ($granted == 0 || $granted == ''){
            $res = ['statusCode' => 404, 'errorMessage' => 'UserToken Not Found or Invalid!'];        
        }else{            
            if (!empty($id) && !empty($takeUserPhoneNumber)) {
                $sql = "SELECT * FROM tb_newlocker_express WHERE deleteFlag = '0' AND id='".$id."'";
                $r = DB::select($sql);

                if (count($r) != 0 ) {
                    $expressNumber = $r[0]->expressNumber;
                    DB::table('tb_newlocker_express')
                        ->where('id', $id)
                            ->update(array(
                                'takeUserPhoneNumber' => $takeUserPhoneNumber,
                                'lastModifiedTime' => time() * 1000
                                ));

                    $res = ['response'=> ['code' => 200, 'message' => 'OK'],  'data' => ['id' => $id, 'expressNumber' => $expressNumber, 'status' => 'PHONE NUMBER CHANGED']];

                } else {

                    $res = ['statusCode' => 401, 'errorMessage' => 'Express Id Not Found!'];
                
                }

            } else {

                $res = ['statusCode' => 501, 'errorMessage' => 'Missing parameter!'];
                
            }

        }

        //insert ke tabel generallog
        DB::table('tb_newlocker_generallog')
            ->insert([
                'api_url' =>  env('APP_URL').'/express/modifyPhoneNumber',
                'api_send_data' => json_encode(['id' => $id, 'takeUserPhoneNumber' => $takeUserPhoneNumber, 'expressNumber' => $expressNumber]),
                'api_response' => json_encode($res),
                'response_date' => date("Y-m-d H:i:s")
            ]);

        return response()->json($res);
    }

    public function deleteImportedExpress (Request $req, $imported) {
        $userToken = $req->header('UserToken');
        $sqltok = "SELECT count(*) AS token FROM tb_newlocker_token WHERE deleteFlag = '0' AND token ='".$userToken."'";
        $rtok = DB::select($sqltok);
        $granted = $rtok[0]->token;
        $deleted = 0;

        if ($granted == 0 || $granted == ''){
                $res = ['response'=> ['code' => 404, 'message' => 'Unauthorized'],  'data' => []];
        }else{
            $express_id = $imported;
            $sqle = "SELECT expressNumber, customerStoreNumber FROM tb_newlocker_express WHERE deleteFlag = '0' AND status = 'IMPORTED' AND id = '".$express_id."'";
            $re = DB::select($sqle);

            if (isset($imported) && count($re) != 0) {                
                $expressNumber = $re[0]->expressNumber;
                $customerStoreNumber = $re[0]->customerStoreNumber;
                DB::table('tb_newlocker_express')
                    ->where('id', $express_id)
                        ->update(array(
                            'deleteFlag' => 1,
                            'lastModifiedTime' => time() * 1000
                ));
                $res = ['response'=> ['code' => 200, 'message' => 'OK'],  'data' => ['id' => $express_id, 'expressNumber' => $expressNumber, 'customerStoreNumber' => $customerStoreNumber, 'status' => 'EXPRESS NUMBER IS DEACTIVATED!']];
                $deleted = 1;
            } else {
                $res = ['response'=> ['code' => 404, 'message' => 'NOT FOUND'],  'data' => []];
            }

            if ($deleted==1){
                //insert ke tabel generallog
                DB::table('tb_newlocker_generallog')
                        ->insert([
                            'api_url' =>  env('APP_URL').'/express/deleteImportedExpress/'.$imported,
                            'api_send_data' => json_encode(['expressId' => $imported]),
                            'api_response' => json_encode($res),
                            'response_date' => date("Y-m-d H:i:s")
                ]);
            }
        }

        return response()->json($res);
    }
    
    public function resycnExpressByTime(Request $req){
        $userToken = $req->header('userToken');
        $endTime = $req->json('endTime');
        $startTime = $req->json('startTime');
        //$urlsms = 'http://smsdev.popbox.asia/sms/send/nexmo';
        $urlmirror = $this->internal_api.'/synclocker/courierstore';

        $sqltok = "SELECT count(*) AS token FROM tb_newlocker_token WHERE deleteFlag = '0' AND token ='".$userToken."'";
        $rtok = DB::select($sqltok);
        $granted = $rtok[0]->token;

        if ($granted == 0 || $granted == ''){
            $res = ['statusCode' => 404, 'errorMessage' => 'UserToken Not Found or Invalid!'];        
        }else{            
            if (!empty($startTime) && !empty($endTime) && $endTime > $startTime) {
                $sql = "SELECT a.id, a.expressNumber, a.takeUserPhoneNumber, a.validateCode, a.storeTime, a.status, a.overdueTime, a.takeTime, b.number, c.name as door_name, d.displayname, e.name as locker_name FROM tb_newlocker_express a, tb_newlocker_mouth b, tb_newlocker_mouthtype c, tb_newlocker_user d, tb_newlocker_box e WHERE a.expressType='COURIER_STORE' and a.groupName is null and a.storeTime > ".$startTime." and a.storeTime < ".$endTime." and a.storeUser_id=d.id_user and a.mouth_id=b.id_mouth and b.mouthType_id=c.id_mouthtype and b.box_id=e.id order by a.storeTime ASC";
                $expresses = DB::select($sql);

                if (count($expresses) != 0 ) {
                    //$statusses = array();
                    $param_express = array();
                    foreach ($expresses as $express => $value) {
                        $param = array();
                        $param['id'] = $value->id; 
                        $param['expressNumber'] = $value->expressNumber; 
                        $param['takeUserPhoneNumber'] = $value->takeUserPhoneNumber; 
                        $param['storeUser']['name'] = $value->displayname; 
                        $param['mouth']['box']['name'] = $value->locker_name; 
                        $param['mouth']['number'] = $value->number; 
                        $param['mouth']['mouthType']['name'] = $value->door_name; 
                        $param['storeTime'] = $value->storeTime; 
                        $param['overdueTime'] = $value->overdueTime; 
                        $param['validateCode'] = $value->validateCode;
                        $param['status'] = $value->status;
                        if (strpos($param['status'], 'TAKEN') != false){
                            $param['takeTime'] = $value->takeTime;                            
                        }

                        array_push($param_express, $param);
                        
                        //re-push to mirrow server
                        $sendParam = $this->post_data($urlmirror, json_encode($param));
                        //$sendParam['post'] = $param;

                        //array_push($statusses, $sendParam);
                        continue;
                        /*                        
                        $overduetimesms = date('j-n-y', $value->overdueTime / 1000);

                        $message = "Kode PIN: " . $value->validateCode . "\nOrder No: " . $value->expressNumber . " sudah tiba di PopBox@" . $value->locker_name . ". Harap diambil sebelum " . $overduetimesms . " - www.popbox.asia";            
                        
                        $sms = json_encode(['to' => $value->takeUserPhoneNumber,
                                'message' => $message,
                                'token' => '2349oJhHJ20394j2LKJO034823423'
                        ]);

                        //re-send sms
                        $sendSMS = $this->post_data($urlsms, $sms);
                        */
                    }

                    //to mitigate last record not pushed
                    $this->post_data($urlmirror, json_encode(end($param_express)));

                    $res = ['response'=> ['code' => 200, 'message' => 'OK'],  'data' => ['total' => count($expresses).' DATA(s) RESYNCED', 'detail' => $param_express]];

                }else{
                    $res = ['statusCode' => 401, 'errorMessage' => 'No Data Found!'];
                }

            } else {
                $res = ['statusCode' => 501, 'errorMessage' => 'Missing parameter or Not Valid Time Range!'];
            } 
        }

        return response()->json($res);
    }

    public function searchExpress (Request $req) {
        $userToken = $req->header('UserToken');
        $sqltok = "SELECT count(*) AS token FROM tb_newlocker_token WHERE deleteFlag = '0' AND token ='".$userToken."'";
        $rtok = DB::select($sqltok);
        $granted = $rtok[0]->token;
        $like = $req->json('like');
        $type = $req->json('lastmile');

        if ($granted == 0 || $granted == ''){
            $res = ['statusCode' => 404, 'errorMessage' => 'UserToken Not Found or Invalid!'];        
        }else{            
            if (!empty($like) && isset($type)){

                if ($type){
                    $sql = "SELECT a.id, a.expressNumber, a.takeUserPhoneNumber, a.validateCode, a.storeTime, a.takeTime, a.expressType, a.status, a.groupName, a.overdueTime, b.number, c.name as size, d.displayname as courier, e.name as locker FROM tb_newlocker_express a, tb_newlocker_mouth b, tb_newlocker_mouthtype c, tb_newlocker_user d, tb_newlocker_box e WHERE a.expressNumber LIKE '%".$like."%' and a.status <> 'IMPORTED' and a.storeUser_id=d.id_user and a.mouth_id=b.id_mouth and b.mouthType_id=c.id_mouthtype and b.box_id=e.id order by a.storeTime ASC";
                } else {
                    $sql = "SELECT a.id, a.customerStoreNumber, a.takeUserPhoneNumber, a.storeUserPhoneNumber, a.expressType, a.storeTime, a.takeTime, a.status, a.groupName, a.endAddress, a.staffTakenUser_id as takeUser, b.number, c.name as size, d.displayname as courier, e.name as locker FROM tb_newlocker_express a, tb_newlocker_mouth b, tb_newlocker_mouthtype c, tb_newlocker_user d, tb_newlocker_box e WHERE a.customerStoreNumber LIKE '%".$like."%' and a.status <> 'IMPORTED' and a.mouth_id=b.id_mouth and b.mouthType_id=c.id_mouthtype and b.box_id=e.id and a.staffTakenUser_id=d.id_user order by a.storeTime ASC";
                }

                $expresses = DB::select($sql);

                if (count($expresses) != 0 ) {
                    $express_ = array();

                    foreach ($expresses as $express => $value) {
                        $param = array();
                        $param['id'] = $value->id; 
                        if($value->expressType=='COURIER_STORE'){
                            $param['expressNumber'] = $value->expressNumber; 
                            $param['overdueTime'] = date('Y-m-d H:i:s', $value->overdueTime / 1000);
                            $param['validateCode'] = $value->validateCode;                           
                            $param['dropCourier'] = $value->courier; 
                        } else {
                            if (!empty($value->endAddress)) {
                                $param['address'] = $value->endAddress;
                            }
                            $param['customerNumber'] = $value->customerStoreNumber; 
                            $param['storeUserNumber'] = $value->storeUserPhoneNumber; 
                            $param['takeCourier'] = $value->courier;
                        } 
                        $param['type'] = $value->expressType;
                        $param['groupName'] = $value->groupName;
                        $param['status'] = $value->status;
                        $param['phoneNumber'] = $value->takeUserPhoneNumber;
                        $param['locker'] = $value->locker; 
                        $param['number'] = $value->number; 
                        $param['size'] = $value->size; 
                        $param['storeTime'] = date('Y-m-d H:i:s', $value->storeTime / 1000);
                        $param['takeTime'] = (!empty($value->takeTime)) ?  date('Y-m-d H:i:s', $value->takeTime / 1000) : null;

                        array_push($express_, $param);
                    }

                    $res = ['total' => count($expresses), 'data' => $express_];

                } else {

                    $res = ['statusCode' => 401, 'errorMessage' => 'Express Id Not Found!'];

                }

            } else {

                $res = ['statusCode' => 501, 'errorMessage' => 'Missing parameter!'];
            }

        }

        return response()->json($res);
    }

    public function query (Request $req) {
        $userToken = $req->header('UserToken');
        $sqltok = "SELECT count(*) AS token FROM tb_newlocker_token WHERE deleteFlag = '0' AND token ='".$userToken."'";
        $rtok = DB::select($sqltok);
        $granted = $rtok[0]->token;
        $expressId=  $_GET['expressId'];
        //echo 'id => : '.$id;

        if ($granted == 0 || $granted == ''){
            $res = ['statusCode' => 404, 'errorMessage' => 'UserToken Not Found or Invalid'];        
        }else{            
            if (!empty($expressId)){
                /* COURIER_STORE
                $id = $locker_activity["id"];
                $name = $locker_activity["storeUser"]["name"]; 
                $barcode = $locker_activity["expressNumber"];
                $phone = $locker_activity["takeUserPhoneNumber"];
                $locker_name = $locker_activity ["mouth"] ["box"] ["name"];
                $locker_number = $locker_activity ["mouth"] ["number"];
                $locker_size = $locker_activity ["mouth"] ["mouthType"] ["name"];
                $storetime =  date('Y-m-d H:i:s', $locker_activity["storeTime"]/1000);
                $overduetime = date('Y-m-d H:i:s', $locker_activity["overdueTime"]/1000);
                $status = $locker_activity["status"];
                $validatecode =  $locker_activity["validateCode"];*/

                /*CUSTOMER_STORE
                $id = $locker_activity["id"];
                $tracking_no = $locker_activity["customerStoreNumber"];
                $phone_number = $locker_activity ["takeUserPhoneNumber"];
                $locker_name = $locker_activity ["mouth"] ["box"] ["name"];
                $locker_number = $locker_activity ["mouth"] ["number"];
                $locker_size = $locker_activity ["mouth"] ["mouthType"] ["name"];
                $storetime =  date('Y-m-d H:i:s', $locker_activity["storeTime"]/1000);
                $status = $locker_activity["status"];
                $taketime = date('Y-m-d H:i:s', $locker_activity["takeTime"]/1000);
                */

                /* CUSTOMER_REJECT
                $id = $locker_activity["id"];
                $tracking_no = $locker_activity["customerStoreNumber"];
                $merchant_name = $locker_activity["electronicCommerce"]["name"];
                $phone_number = $locker_activity ["storeUser"]["name"];
                $locker_name = $locker_activity ["mouth"] ["box"] ["name"];
                $locker_number = $locker_activity ["mouth"] ["number"];
                $locker_size = $locker_activity ["mouth"] ["mouthType"] ["name"];
                $storetime =  date('Y-m-d H:i:s', $locker_activity["storeTime"]/1000);
                $status = $locker_activity["status"];
                $taketime =  date('Y-m-d H:i:s', $locker_activity["takeTime"]/1000);
                */

                $checkTypeId = DB::select("SELECT expressType FROM tb_newlocker_express WHERE id = '".$expressId."'");
                $typeExpress = $checkTypeId[0]->expressType;

                if (count($checkTypeId) != 0){

                    if ($typeExpress=='CUSTOMER_REJECT') {
                        $sql = "SELECT a.id, a.customerStoreNumber, a.storeTime, a.status, a.takeTime, a.storeUserPhoneNumber, a.groupName, a.electronicCommerce_id, b.number, c.name as door_name, e.name as locker_name FROM tb_newlocker_express a, tb_newlocker_mouth b, tb_newlocker_mouthtype c, tb_newlocker_box e WHERE a.id = '".$expressId."' AND a.mouth_id=b.id_mouth AND b.mouthType_id=c.id_mouthtype AND b.box_id=e.id;";                        
                    } else if ($typeExpress=='CUSTOMER_STORE') {
                        $sql = "SELECT a.id, a.customerStoreNumber, a.storeTime, a.status, a.takeTime, a.takeUserPhoneNumber, a.groupName, b.number, c.name as door_name, e.name as locker_name FROM tb_newlocker_express a, tb_newlocker_mouth b, tb_newlocker_mouthtype c, tb_newlocker_box e WHERE a.id = '".$expressId."' AND a.mouth_id=b.id_mouth AND b.mouthType_id=c.id_mouthtype AND b.box_id=e.id;";                       
                    } else {
                        $sql = "SELECT a.id, a.expressNumber, a.takeUserPhoneNumber, a.validateCode, a.storeTime, a.status, a.overdueTime, a.takeTime, a.groupName, b.number, c.name as door_name, d.displayname, e.name as locker_name FROM tb_newlocker_express a, tb_newlocker_mouth b, tb_newlocker_mouthtype c, tb_newlocker_user d, tb_newlocker_box e WHERE a.id = '".$expressId."' AND a.storeUser_id=d.id_user AND a.mouth_id=b.id_mouth AND b.mouthType_id=c.id_mouthtype AND b.box_id=e.id;";                         
                    }

                    $expresses = DB::select($sql);

                    $result = array();
                    foreach ($expresses as $express) {
                        $param = array();
                        $param['id'] = $express->id; 
                        $param['expressType'] = $typeExpress; 
                        $param['storeTime'] = $express->storeTime; 
                        $param['status'] = $express->status;
                        if (strpos($param['status'], 'TAKEN') != false){
                            $param['takeTime'] = $express->takeTime;                            
                        }

                        $param['groupName'] = $express->groupName;                        
                        if ($typeExpress=='CUSTOMER_REJECT') {
                            $param['customerStoreNumber'] = $express->customerStoreNumber; 
                            $param['storeUser']['name'] = $express->storeUserPhoneNumber;
                            if (empty($express->groupName)) {
                                $checkEcommerce = DB::select("SELECT company_name FROM tb_newlocker_company WHERE id_company = '".$express->electronicCommerce_id."'");
                                $param['electronicCommerce']['name'] = $checkEcommerce[0]->company_name;
                            } else {
                                $param['electronicCommerce']['name'] = $express->groupName;
                            }
                        } else if ($typeExpress=='CUSTOMER_STORE') {
                            $param['customerStoreNumber'] = $express->customerStoreNumber;
                            $param['takeUserPhoneNumber'] = $express->takeUserPhoneNumber; 
                        } else {
                            $param['overdueTime'] = $express->overdueTime; 
                            $param['validateCode'] = $express->validateCode;
                            $param['expressNumber'] = $express->expressNumber; 
                            $param['takeUserPhoneNumber'] = $express->takeUserPhoneNumber; 
                            $param['storeUser']['name'] = $express->displayname; 
                        }
                        $param['mouth']['box']['name'] = $express->locker_name; 
                        $param['mouth']['number'] = $express->number; 
                        $param['mouth']['mouthType']['name'] = $express->door_name; 

                        array_push($result, $param);
                    }
                    $res = ['page' => 0, 'maxCount' => 10, 'totalPage' => 0, 'totalCount' => count($result), 'resultList'=> $result];

                } else {

                        $res = ['page' => 0, 'maxCount' => 10, 'totalPage' => 0, 'totalCount' => 0, 'resultList' => []];
                    }

            } else {

                $res = ['statusCode' => 501, 'errorMessage' => 'Missing parameter!'];
            }

        }

        return response()->json($res);
    }

    public function queryImported (Request $req) {
        $userToken = $req->header('UserToken');
        $sqltok = "SELECT count(*) AS token FROM tb_newlocker_token WHERE deleteFlag = '0' AND token ='".$userToken."'";
        $rtok = DB::select($sqltok);
        $granted = $rtok[0]->token;
        $expressType=  $_GET['expressType'];
        $expressStatus=  $_GET['expressStatus']; // Force ONLY IMPORTED
        $maxCount=  $_GET['maxCount'];
        //echo 'id => : '.$id;

        if(empty($maxCount)){
            $maxCount = 100;
        }

        if ($granted == 0 || $granted == ''){
            $res = ['statusCode' => 404, 'errorMessage' => 'UserToken Not Found or Invalid'];        
        }else{            
            if (!empty($expressType) && $expressStatus == 'IMPORTED'){ 
                $sql = "SELECT * FROM tb_newlocker_express WHERE expressType = '".$expressType."' AND status = 'IMPORTED' ORDER BY importTime DESC LIMIT 0,".$maxCount;

                $expresses = DB::select($sql);

                    $result = array();
                    foreach ($expresses as $express) {
                        $param = array();
                        $param['id'] = $express->id; 
                        $param['expressType'] = $express->expressType;
                        $param['createTime'] = $express->importTime; 
                        $param['status'] = $express->status;
                        $param['groupName'] = $express->groupName;                        
                        $param['takeUserPhoneNumber'] = $express->takeUserPhoneNumber; 
                        $param['item'] = []; 
                        if ($expressType=='COURIER_STORE') {
                            $param['expressNumber'] = $express->expressNumber; 
                        } else {
                            $param['customerStoreNumber'] = $express->customerStoreNumber;
                            $param['storeUserPhoneNumber'] = $express->storeUserPhoneNumber;
                        } 
                        array_push($result, $param);
                    }

                    $res = ['page' => 0, 'maxCount' => (int)$maxCount, 'totalPage' => 0, 'totalCount' => count($result), 'resultList'=> $result];

            } else {
                $res = ['statusCode' => 501, 'errorMessage' => 'Missing parameter!'];
            }

        }

        return response()->json($res);
    }

    public function createPayment($params, $expressNumber)
    {
        // Try to create payment
        $payment  = new PaymentAPI;
        $sendData = $payment->createPayment((array)$params);
        
        if ($sendData->response->code == 200) {
            DB::table('tb_history_payment')
                ->insert([
                    'transaction_id' => $params->transaction_id,
                    'expressNumber' => $expressNumber,
                    'payment_id' => $sendData->data[0]->payment_id,
                    'locker_name'  => $params->location_name,
                    'payment_method'  => $params->method_code,
                    'amount'  => $params->amount
            ]);
        }

        return json_encode($sendData);
    }

    public function createPopsafe($req, $paymentParam, $lockerIdentity, $isExtend=false, $status_op_taken)
    {

        $syncFlag = $req->json('syncFlag');
        $box_id = $req->json('box_id');
        $overduetime = $req->json('overdueTime');
        $mouth_id = $req->json('mouth_id');
        $expressType = $req->json('expressType');
        $logisticsCompany_id = $req->json('logisticsCompany_id');
        $version = $req->json('version');
        $id = $req->json('id');
        $storeUser_id = $req->json('storeUser_id');
        $status = $req->json('status');
        $groupName = $req->json('groupName');
        $takeUserPhoneNumber = $req->json('takeUserPhoneNumber');
        $storeTime = $req->json('storeTime');
        $validateCode = $req->json('validateCode');
        $operator_id = $req->json('operator_id');
        $expressNumber = $req->json('expressNumber');
        $overduetimesms = $req->json('overdueTime');
        $transactionRecord = (object)$req->json('transactionRecords')[0];
        $smsSend = true;
        
        $prefix = substr($expressNumber,0,3);
        $rpr = DB::select("SELECT groupName, sms FROM tb_newlocker_groupname WHERE prefix='".$prefix."'");
      
        if (count($rpr) != 0) {
            $groupName = $rpr[0]->groupName;
            $smsSend = $rpr[0]->sms;
        }else{
            $groupName = 'UNDEFINED';
        }

        $checkExpress = $this->checkExpressNumber($expressNumber);
        if (!$isExtend && empty($checkExpress)) {
            if($smsSend==true||$smsSend==1){
                if($operator_id == '145b2728140f11e5bdbd0242ac110001'){
                    if (substr($expressNumber, 0, 3) != 'PDS'){
                        $overduetimesms = date('d/m/y H:i', $overduetime / 1000);
                        $message = "Kode buka titipan Anda: " . $validateCode . " untuk order no ". $expressNumber ." di PopBox @" . $paymentParam->location_name . " ".$lockerIdentity['lockerName']."/".$lockerIdentity['lockerNo']. " Valid sampai " . $overduetimesms;  
                        $cekSms = $this->checkSms($id);
                        if (empty($cekSms)){
                            $resp = $this->sendFromNexmo($takeUserPhoneNumber, $message);    
                            $statusMsg = (strpos(str_replace(' ', '', $resp->getData()), '"status":"0"') != false) ? 'SUCCESS' : 'FAILED' ;
                            $msg = $expressNumber ." - sms : $resp \n";
                            $name = 'sms';
                            $this->writeLogs($msg, $name);
                            DB::table('tb_newlocker_smslog')
                                ->insert([
                                    'express_id' => $id,
                                    'sms_content' => '[Auto] '.$message,
                                    'sms_status' => $statusMsg,
                                    'sent_on' => date("Y-m-d H:i:s"),
                                    'original_response' => json_encode($resp)
                            ]); 
                        }
                        
                    }
                }
                // else{
                //     $urlsms = "http://pr0x-my.popbox.asia/task/express/resendSMS";        
                //     if (!in_array('userToken', $this->headers)) {
                //         $this->headers[]='userToken:22cc0a50e199494682a9fc2ee2e88294';
                //     }  
                //     $this->post_data($urlsms, json_encode(['id' => $id]), $this->headers);
                // }
            }
        }
        $resp__ = [];
        $url__ = '';
        if($groupName=="POPDEPOSIT" || $groupName == "POPTITIP" || $groupName == "AP_EXPRESS" || $groupName == "EVENT_POPBOX"){
            if ( (substr($expressNumber, 0, 3) == "PDL") || (substr($expressNumber, 0, 3) == "PSL") || (substr($expressNumber, 0, 5) == "APXID")  ){  
                // perubahan riwandy 
                $url = env('POPSEND2_URL');
                $token__ = env('POPSEND2_TOKEN'); 


                if (!$isExtend && $status_op_taken == false) {
                    $paymentMethod = $paymentParam->method_code;
                    if ($paymentMethod == "MANDIRI-EMONEY") {
                        $paymentMethod = "EMONEY";
                    }
                    
                    $amount = isset($paymentParam->amount) ? $paymentParam->amount : $transactionRecord->amount;
                    $url__ = $url.'popsafe-locker/order';
                    $param__ = ["token" => $token__, "expressNumber" => $expressNumber, "takeUserPhoneNumber" => $takeUserPhoneNumber,
                        "overdueTime" => $overduetime, "id" => $id, "box_id" => $box_id, "storeTime" => $storeTime, "validateCode" => $validateCode,
                        "lockerSize" => $lockerIdentity['lockerName'], "lockerNo" => $lockerIdentity['lockerNo'], "paymentMethod" => $paymentMethod,
                        "paymentAmount" => $amount, "transactionRecord" => $paymentParam->transaction_id ];
                } elseif ($isExtend == true || $status == 'CUSTOMER_TAKEN' || $status == 'OPERATOR_TAKEN') {
                    $dataExtend = ($req->json('dataExtend') != null) ? json_decode($req->json('dataExtend')) : $req->getContent();
                    $url__ = $url.'popsafe/updateStatus';
                    $param__ = ["token" => $token__, "invoice_id" => $expressNumber, "status" => $status, "locker_number" => $lockerIdentity['lockerNo'], "code_pin" => $validateCode, "parcel_id" => $id, "remarks" => "[ Sync from locker ] Parcel Is Extend By User On : ". date("Y-m-d H:i:s"), "request" => $dataExtend];
                }

                if ($req->json('dataOperator') != null) {
                    $param__['dataOperator'] = $req->json('dataOperator');                
                } else {
                    $param__['dataOperator'] = null;                
                }   

                $resp__ = $this->post_data($url__, json_encode($param__));
            }

            DB::table('tb_newlocker_generallog')->insert(
                ['api_url' =>  $url__, 
                'api_send_data' => json_encode($param__),
                'api_response' => json_encode($resp__),
                'response_date' => date("Y-m-d H:i:s")]); 
        }
        return response()->json($resp__);
    }

    public function sendFromNexmo($to, $message){
        $response = ['response' => ['code' => 500, 'message' => 'ERROR'], 'data' => []];
        if(!empty($to) && !empty($message)) {
            if(substr($to, 0, 1) == '+') {
                $to = substr($to, 1);
            } else if(substr($to, 0, 1) == '0') {
                $to = '62'.substr($to, 1);
            } else if(substr($to, 0, 1) == '8') {
                $to = '62'.$to;
            } else if(substr($to, 0, 2) == '01') { //Can also handle Malaysia Phone Number. [Wahyudi 09-09-17] for Malaysia Backup
                $to = '6'.$to;
            }
            $data = array();
            $data["api_key"] = '5b582569';
            $data["api_secret"] = 'f1708d28f0dfaffa';
            $data["from"] = 'POPBOX-ASIA';
            $data["to"] = $to;
            $data["text"] = $message;
            $url = 'https://rest.nexmo.com/sms/json?' . http_build_query($data);
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = json_decode(curl_exec($ch), TRUE);
            $response['raw'] = $response;
            $response = json_encode($response);
            DB::table('tb_newlocker_generallog')
                ->insert([
                    'api_url' => $url,
                    'api_send_data' => json_encode($data),
                    'api_response' => $response,
                    'response_date' => date("Y-m-d H:i:s")
                ]);
        }
        return response()->json($response);      
    }

    public function writeLogs($message, $name)
    {
        $date = date('Y.m.d');
        $time = date('H:i:s');
        $msg  = ">> $time $message";
        $f = fopen(storage_path().'/logs/sync/'.$name.'.'.$date.'.log','a');
        fwrite($f,$msg);
        fclose($f);
    }

    public function checkExpressNumber($expressNumber)
    {
        $data = DB::table('tb_newlocker_express')->where('expressNumber', $expressNumber)->first();
        return $data;
    }

    public function checkSms($id)
    {
        $data = DB::table('tb_newlocker_smslog')->where('express_id', $id)->where('sms_content', 'like', '[AUTO]%')->first();
        return $data;
    }

    public function sms_content($groupName, $param){
        $content = "Open Key: {validateCode} Collect your order {expressNumber} at PopBox Locker@{boxName} before {overdueDayOfMouth}/{overdueMouth}/17 bit.do/getpopbox";
        
        if ($param['operator_id'] == '145b2728140f11e5bdbd0242ac110001'){
            $content = "Kode buka: {validateCode} order no: {expressNumber} sudah tiba di PopBox@{boxName}. Valid s/d: {overdueDayOfMouth}/{overdueMouth}/17 CS: 02122538719";
        }
        
        if (isset($groupName) && ($groupName != 'UNDEFINED' || !empty($groupName))) {
            $sqlsms = "SELECT a.content, b.name FROM tb_newlocker_smstemplate a, tb_newlocker_grouptemplate b WHERE a.smsType <> 'REJECT_EXPRESS_STORE_NOTIFY_CUSTOMER' AND b.id = a.templateGroup_id AND b.name ='".$groupName."'";
            $rsms = DB::select($sqlsms);

            if (count($rsms) != 0 ) {
                $content = $rsms[0]->content;  
            } 
        }

        $validateCode = $param['validateCode'];
        $expressNumber = $param['expressNumber'];
        $box_name = $param['box_name'];
        $overduetimesms = $param['overduetimesms'];
        //==================================================================================================//
        $content_ = str_replace('{validateCode}', $validateCode, $content);
        $content__ = str_replace('{expressNumber}', $expressNumber, $content_);
        $content___ = str_replace('{boxName}', $box_name, $content__);
        $content____ = str_replace('{overdueDayOfMouth}/{overdueMouth}/17', $overduetimesms, $content___);

        return $content____;
    }

    public function blockParcel(Request $request)
    {
        $response = new \stdClass();
        $response->isSuccess = false;
        $response->data = null;

        $parcel_id     = $request->input('parcel_id');
        $validate_code = $request->input('validate_code');
        $is_block      = $request->input('is_block');
        if ($is_block) {
            $parcelDb = DB::table('tb_newlocker_express')->find($parcel_id);
            if ($parcelDb) {
                if ($parcelDb->status != 'IN_STORE') {
                    $response->message = 'Data Must Be IN STORE';
                    return response()->json($response);   
                } else {
                    $expressBlockTb = new ExpressBlock;
                    $result = $expressBlockTb->blocking($parcel_id, $validate_code);
                    if ($result->isSuccess) {
                        $response->isSuccess = true;
                        $response->message = 'Blocking Success, Please wait data will sync from Server to Locker Machine';
                        $this->blockUnBlockPopSafe($parcel_id, 'BLOCKED');
                        return response()->json($response);   
                    } else {
                        $response->message = $result->message;
                        return response()->json($response, 500);   
                    }
                }      
            } else {
                $response->message = "Data Not Found";
                return response()->json($response, 404);
            }
        } else {
            $expressBlockTb = new ExpressBlock;
            $result = $expressBlockTb->unblocking($parcel_id, $validate_code);
            if ($result->isSuccess) {
                $response->isSuccess = true;
                $response->message = 'Unblocking Success, Please wait data will sync from Server to Locker Machine';
                $this->blockUnBlockPopSafe($parcel_id, $validate_code);
                return response()->json($response);   
            } else {
                $response->message = $result->message;
                return response()->json($response, 500);   
            }
        }

    }

    private function blockUnBlockPopSafe($parcel_id, $validate_code)
    {
        // COMMENT BECAUSE WILL CHANGE CODE_PIN IN POPSAFE TABLE
        // $url__ = env('POPSEND2_URL').'popsafe-locker/block';
        // $token__ = env('POPSEND2_TOKEN'); 
        // $express = DB::table('tb_newlocker_express')->find($parcel_id);
        // $param   = $param__ = ["token" => $token__, "expressNumber" => $express->expressNumber, "validateCode" => $validate_code];
        // $resp__ = $this->post_data($url__, json_encode($param));

        // DB::table('tb_newlocker_generallog')->insert([
        //     'api_url' => $url__,
        //     'api_send_data' => json_encode($param),
        //     'api_response' => json_encode($resp__),
        //     'response_date' => date("Y-m-d H:i:s")
        // ]);
        // return $resp__;
        $this->addTaskBlocking($parcel_id, $validate_code);
    }

    private function addTaskBlocking($parcel_id, $validate_code)
    {
        $response = new \stdClass();
        $response->isSuccess = false;

        $parcelDb = DB::table('tb_newlocker_express')->find($parcel_id);
        DB::beginTransaction();
        try {
            DB::table('tb_newlocker_express')->where('id', $parcel_id)->update([
                'validateCode' => $validate_code,
                'lastModifiedTime' => time() * 1000
            ]);
    
            $timestamp = time() * 1000;
            $id_task = hash("haval128,5", $timestamp);
    
            DB::table('tb_newlocker_tasks')
                ->insert([
                    'id' => $id_task,
                    'box_id' => $parcelDb->box_id,
                    'expressId' => $parcel_id,
                    'status' => 'COMMIT',
                    'task' => 'RESET_EXPRESS',
                    'messageType' => 'ASYNC_TASK',
                    'createTime' => $timestamp,
                    'mouth_id' => $parcelDb->mouth_id
                ]);
            DB::commit();
            $response->isSuccess = true;
            return $response;
        } catch (\Exception $e) {
            DB::rollBack();
            return $e->getMessage();
        }
    }

    public function inquiryExpressTransit(Request $request)
    {
        $response = new \stdClass();
        $response->code = "200";

        $boxToken = $request->header('BoxToken');
        $diskSerialNumber = $request->header('DiskSerialNumber');
        $orderNo = $request->header('OrderNo');
        $userToken = $request->header('UserToken');
        $parcel_id = $request->input('parcel_id');
        $express_number = $request->input('express_number');

        $dataTransit = PopExpressTransit::where('parcel_id', $parcel_id)->where('express_number', $express_number)->first();
        
        if (!$dataTransit) {
            $response->message = "Please wait, Data has not been synced from locker";
        } else {
            $param = (array)json_decode($dataTransit->param_request);
            $response->data = $param;
        }
        return json_encode($response);
    }

    public function requestPin(Request $request)
    {
        $commands = explode("#", $request->input('commands'));
        $handphone = $commands[0];
        $expressNumber = $commands[1];

        $data = DB::table('tb_newlocker_express')
                ->where('takeUserPhoneNumber', $handphone)
                ->where('expressNumber', $expressNumber)
                ->first();

        if ($data) {
            $message = "Data Paket Kamu : \n\nNo. Parcel : $data->expressNumber \nStatus : $data->status \nPIN : $data->validateCode \n\nTerima Kasih.";

        } else {
            $message = "Maaf! Data kamu tidak ditemukan";
        }

        $msg = [
            "message" => $message
        ];

        return json_encode($msg);
    }

    public function popsafeCreateSync(Request $request)
    {
        $box_id = $request->input('box_id');
        $paymentParam = json_decode(json_decode($request->input('paymentParam')));
        $expressNumber = $request->input('expressNumber');
        $id = $request->input('id');
        $locker_identity = $request->input('locker_identity');
        $overdueTime = $request->input('overdueTime');
        $transactionRecords = $request->input('transactionRecords');
        $storeTime = $request->input('storeTime');
        $takeUserPhoneNumber = $request->input('takeUserPhoneNumber');
        $validateCode = $request->input('validateCode');
        $data = [
            "box_id" => $box_id,
            "expressNumber" => $expressNumber,
            "id" => $id,
            "lockerNo" => $locker_identity['lockerNo'],
            "lockerSize" => $locker_identity['lockerName'],
            "overdueTime" => $overdueTime,
            "paymentAmount" => $transactionRecords[0]['amount'],
            "paymentMethod" => $transactionRecords[0]['paymentType'],
            "storeTime" => $storeTime,
            "takeUserPhoneNumber" => $takeUserPhoneNumber,
            "transactionRecord" => $paymentParam->transaction_id,
            "validateCode" => $validateCode,
            "token" => "LWPU0G5UFGI711XPAETGKC9CAZZT1AKTQPONWCLK7NUV8KJKD1"
        ];

        return json_encode($data);   
    }

    public function popsafeCreate(Request $request) {
        $box_id = $request->input('box');
        $expressNumber = $request->input('expressNumber');
        $id = $request->input('id');
        $lockerNo = $request->input('lockerNo');
        $lockerSize = $request->input('lockerSize');
        $overdueTime = $request->input('overdueTime');
        $paymentAmount = $request->input('paymentAmount');
        $paymentMethod = $request->input('paymentMethod');
        $storeTime = $request->input('storeTime');
        $takeUserPhoneNumber = $request->input('takeUserPhoneNumber');
        $transactionRecord = $request->input('transactionRecord');
        $validateCode = $request->input('validateCode');
        $data = [
            "box_id" => $box_id['id'],
            "transactionRecord" => $transactionRecord,
            "expressNumber" => $expressNumber,
            "id" => $id,
            "lockerNo" => $lockerNo,
            "lockerSize" => $lockerSize,
            "overdueTime" => $overdueTime,
            "paymentAmount" => $paymentAmount,
            "paymentMethod" => $paymentMethod,
            "storeTime" => $storeTime,
            "takeUserPhoneNumber" => $takeUserPhoneNumber,
            "transactionRecord" => $transactionRecord,
            "validateCode" => $validateCode,
            "token" => "LWPU0G5UFGI711XPAETGKC9CAZZT1AKTQPONWCLK7NUV8KJKD1"
        ];

        return json_encode($data);
    }
}