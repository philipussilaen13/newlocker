<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class LockerStatus extends Command
{

    var $curl;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lockerstatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create schedule task for set status locker to 0';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */


    public function handle(){
        $rangeTime = time()-300; //where no update for 5 minutes
        $query = "SELECT m.* FROM tb_newlocker_machinestat m, tb_newlocker_box b WHERE m.locker_id = b.id AND b.activationFlag = 1 AND m.locker_id IS NOT NULL ";
        $all_lockers = DB::select($query);
        $offline = 0;
        echo "=== CHECK OFFLINE on ".date("Y-m-d H:i:s")." For Time Range Below : ".date("Y-m-d H:i:s", $rangeTime)."\n";
        if (!empty($all_lockers)) {
            // update status locker menjadi 0
            foreach ($all_lockers as $locker => $value) {
                $locker_id = $value->locker_id;
                $locker_name = $value->locker_name;
                $last_update = $value->update_time;
                if (strtotime($last_update) < $rangeTime){
                    $offline++;
                    DB::table('tb_newlocker_machinestat')
                        ->where('locker_id', $locker_id)
                        ->update(['conn_status' => 0]);
                    
                    echo $offline." - ".$locker_name." [Last Sync: $last_update] \n";
                }
            }
            echo "=== $offline OFFLINE LOCKERS WAS FOUND!\n";
        }
    }
}
