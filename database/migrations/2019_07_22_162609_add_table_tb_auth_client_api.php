<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableTbAuthClientApi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasTable('tb_client_auth_api'))) {
            Schema::create('tb_client_auth_api', function (Blueprint $table) {
                $table->increments('id');
                $table->string('service_name');
                $table->string('service_key');
                $table->string('popbox_header_auth');
                $table->tinyInteger('is_active');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
