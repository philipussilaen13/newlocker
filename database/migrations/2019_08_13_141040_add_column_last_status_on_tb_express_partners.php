<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLastStatusOnTbExpressPartners extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasColumn('tb_express_partners', 'last_status')))
        {
            Schema::table('tb_express_partners', function (Blueprint $table) {
                $table->string('last_status')->nullable()->after('logistics_company');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
