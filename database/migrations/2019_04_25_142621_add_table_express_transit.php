<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableExpressTransit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!(Schema::hasTable('tb_express_transit'))) {
            Schema::create('tb_express_transit', function (Blueprint $table) {
                $table->increments('id');
                $table->string('parcel_id');
                $table->string('express_number');
                $table->tinyInteger('is_transit');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
